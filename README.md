CheckPin
-------------------------
CheckPin - команда для получения уникального ключа, с помощью которого можно использовать другие команды

	CheckPin NUMBERCONNECT -1 <PWD> <EMAIL> <PIN> TYPE
### Параметры
NUMBERCONNECT - номер соединения
PWD - пароль, который получили при регистрации
EMAIL - почтовый адрес, который указывали при авторизации
PIN - пароль, который надо ввести при авторизации по звонку
TYPE - тип соединения. Указывать IPA, если соединение проходит через PHP
### Пример запроса
	CheckPin U1 -1 <> <bubblegumoff@gmail.com> <1234> ipa
### Ответ от сервера
	_StartClient ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw

GetWaletFullInfo
-------------------------
GetWaletFullInfo - команда для получения информации по кошелькам
	GetWaletFullInfo NUMBERCONNECT <UKEY> * PORT
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
PORT - номер порта по которому можно получить информацию по кошельку
*18332 - биткоин кошелёк
*19332 - лайткоин кошелёк
### Пример запроса
	GetWaletFullInfo U1 <ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw> * 18332
### Ответ от сервера
	GetWaletFullInfo ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw {"result":0.00000000,"error":null,"id":"BTCExposed"}

GetOffers
-------------------------
GetOffers - команда для получения строк с биржи
	GetOffers NUMBERCONNECT <UKEY> * *
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
### Пример запроса
	GetOffers U1 <ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw> * * 
### Ответ от сервера
	_GetOffers ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <1 0 1 10 1 43256.5105007639 1>

GetMyTokens
-------------------------
GetMyTokens - команда для получения купленных токенов стартапов
	GetMyTokens NUMBERCONNECT <UKEY>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
### Пример запроса
	GetMyTokens U1 <ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw> 
### Ответ от сервера
	_GetMyTokens <2 1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw>

GetTokenICO 
-------------------------
GetTokenICO - команда для получения информации по токену
	GetTokenICO NUMBERCONNECT <UKEY> <IDTOKEN>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDTOKEN - идентификатор токена
### Пример запроса
	GetTokenICO U1 <ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw> 2 
### Ответ от сервера
	_GetTokenICO <2> <2> <6> <ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw> <1> <100> <900>

GetJsnICOP 
-------------------------
GetJsnICOP - команда для получения информации по стартапу
	GetJsnICOP NUMBERCONNECT * <IDSTARTUP>
### Параметры
NUMBERCONNECT - номер соединения
IDSTARTUP - идентификатор стартапа
### Пример запроса
	GetJsnICOP U1 <ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw> 2 
### Ответ от сервера
	_GetJsnICOP <{"name":"СЃРІРµР¶РёР№ РІРѕР·РґСѓС…","shortDesc":"СЃРѕР±РёСЂР°С‚СЊ РіРѕСЂРЅС‹Р№ РІРѕР·РґСѓС… РІ РїСЏС‚РёР»РёС‚СЂРѕРІС‹Рµ Р±СѓС‚С‹Р»Рё","fullDesc":"СЃРѕР±РёСЂР°С‚СЊ РіРѕСЂРЅС‹Р№ РІРѕР·РґСѓС… РІ РїСЏС‚РёР»РёС‚СЂРѕРІС‹Рµ Р±СѓС‚С‹Р»Рё","problems":"РїСЂРѕР±Р»РµРјР° СЃРІРµР¶РµРіРѕ РІРѕР·РґСѓС…Р°","problemsHow":"РґРѕР»РіРѕ Рё РјСѓС‚РѕСЂРЅРѕ","tech":"РїСЏС‚РёР»РёС‚СЂРѕРІС‹Рµ РџР­Рў Р±СѓС‚С‹Р»РєРё СЃ РІРѕР·РґСѓС…РѕРј","projectState":"2","video":"https:\/\/youtu.be\/B2NsNV-VCxo","website":"www.google.com","uName":"Р¤Р°СЂРёС‚","uPhone":"+79172287103","uSLink":"%D0%BD%D0%B5%D1%82","roadmap":[],"whoCreate":48,"btcAddress":"mn6sRFz1FMUtkB3WMPc4hD6cV3SwQerLJm","ltcAddress":"miMU1QhDeupNwXc8ANeEfq7s6aRWxqyW8w"}> <[{"name":"Р¤РРћ","position":"РіР»Р°РІРЅС‹Р№","contacts":"[]","photo":"http%3A%2F%2Fdesdao.com%2Ftemplate%2Fimages%2Fuser.png","desc":""}]> <[{"name":"РїСѓС€РєРµРЅ","position":"РєСЂРёС‚РµРє","contacts":"[]","photo":"http%3A%2F%2Fdesdao.com%2Ftemplate%2Fimages%2Fuser.png","desc":""}]> <null>

GetNonModerate  
-------------------------
GetNonModerate - команда для получения списка стартапов не прошедших модерацию
	GetNonModerate NUMBERCONNECT * *
### Параметры
NUMBERCONNECT - номер соединения
### Пример запроса
	GetNonModerate U1 * * 
### Ответ от сервера
	_GetNonModerate 2 <0 1>

GetIcoTokenDat1  
-------------------------
GetIcoTokenDat1 - команда для получения информации по токену
	GetNonModerate NUMBERCONNECT * *
### Параметры
NUMBERCONNECT - номер соединения
### Пример запроса
	GetIcoTokenDat1 U1 * 2 
### Ответ от сервера
	_GetIcoTokenDat1 <1> <2> <48> <РіРѕСЂРЅРѕРєРѕРёРЅ> <РіСЂРЅРєРЅ> <РіСЂРЅРє> <100000> <2> <100> <10> <1527785897> <1527811200> <1530403200> <1>

CreateNewAdres   
-------------------------
CreateNewAdres - команда для создания нового адреса
	CreateNewAdres NUMBERCONNECT <UKEY> * <PORT>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
PORT - номер порта по которому можно получить информацию по кошельку
*18332 - биткоин кошелёк
*19332 - лайткоин кошелёк
### Пример запроса
	CreateNewAdres U1 <ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw> * 18332
### Ответ от сервера
	CreateNewAdres ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw {"result":"mk4PKva46EPE1V1KBcUcbuAyhAPV3BapDc","error":null,"id":"BTCExposed"}

RegICOP    
-------------------------
RegICOP - команда для создания нового стартапа
	RegICOP NUMBERCONNECT <UKEY> <1> <1> <STARTUPNAME> <555555> <1000> <43426> <INFOARRAY> <TEAMARRAY> <ADVARRAY> <DOCSARRAY>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
STARTUPNAME - Название стартапа
INFOARRAY - json массив с информацией о стартапе
TEAMARRAY - json массив с командой стартапа
ADVARRAY - json массив с помощниками стартапа
ADVARRAY - json массив с документами стартапа
### Пример запроса
	RegICOP U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <1> <1> <РќР°РёРјРµРЅРѕРІР°РЅРёРµ РїСЂРѕРµРєС‚Р°> <555555> <1000> <43426> <{"name":"РќР°РёРјРµРЅРѕРІР°РЅРёРµ РїСЂРѕРµРєС‚Р°","shortDesc":"РљСЂР°С‚РєРѕРµ РѕРїРёСЃР°РЅРёРµ РїСЂРѕРµРєС‚Р°","fullDesc":"РџРѕРґСЂРѕР±РЅРѕРµ РѕРїРёСЃР°РЅРёРµ РїСЂРѕРµРєС‚Р°","problems":"РџСЂРѕР±Р»РµРјС‹, РєРѕС‚РѕСЂС‹Рµ СЂРµС€Р°РµС‚ РїСЂРѕРµРєС‚","problemsHow":"РљР°РєРёРј РѕР±СЂР°Р·РѕРј СЂРµС€Р°СЋС‚СЃСЏ СЌС‚Рё РїСЂРѕР±Р»РµРјС‹","tech":"РўРµС…РЅРѕР»РѕРіРёСЏ \/ РѕРїРёСЃР°РЅРёРµ РїСЂРѕРґСѓРєС‚Р°","projectState":"3","video":"https:\/\/www.youtube.com\/watch?v=-RZjoHj78fM&amp;t=4s","website":"http:\/\/desdao.com","uName":"РЎРѕРєРѕР»РѕРІ Р’Р»Р°РґРёСЃР»Р°РІ Р’Р°Р»РµСЂСЊРµРІРёС‡","uPhone":"+70123456789","uSLink":"https%3A%2F%2Fwww.instagram.com%2Fbubblegumoff%2F","roadmap":[],"whoCreate":3,"btcAddress":"moA9iaFviQQoYqkGKC536ZaBHmcCZKRME9","ltcAddress":"n4YbLM18X8H2FrBeQ8To3K6QK7wM995B1k"}> <[{"name":"1","position":"2","contacts":"[]","photo":"http%3A%2F%2Fdesdao.com%2Ftemplate%2Fimages%2Fdes%2Fslider-1-photo.png","desc":""}]> <[{"name":"1","position":"2","contacts":"[]","photo":"http%3A%2F%2Fdesdao.com%2Ftemplate%2Fimages%2Fuser.png","desc":""}]> <null>
### Ответ от сервера
	_RegICOP 3

AddNewIcoToken    
-------------------------
AddNewIcoToken - команда для создания нового токена
	AddNewIcoToken NUMBERCONNECT <UKEY> <IDSTARTUP> <FULLNAME> <SHORTNAME> <ABBREVIATURE> <MAXTKN> <DIGITS> <BOUNTY> <PRICE> <DATESTART> <DATEEND> <VALUTE>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
FULLNAME - полное наименование токена
SHORTNAME - краткое наименование токена
ABBREVIATURE - аббревиатура
MAXTKN - число токенов
DIGITS - знаков после запятой
BOUNTY - % команде
PRICE - стоимость токена
DATESTART - дата старта продаж
DATEEND - дата окончания продаж
VALUTE - валюта продажи токенов
*1 - доллары
### Пример запроса
	AddNewIcoToken U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <3> <Р’Р»Р°РґРёСЃР»Р°РІ> <Р’Р»Р°Рґ> <Р’Р›Р”> <100000> <12> <25> <13> <06.06.2018 00:00:00> <30.06.2018 00:00:00> <1>
### Ответ от сервера
	_AddNewIcoToken ok

WriteJsnRoadmap     
-------------------------
WriteJsnRoadmap - команда для создания дорожной карты
	WriteJsnRoadmap NUMBERCONNECT <UKEY> <IDSTARTUP> <INFOARRAY>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
INFOARRAY - json массив с информацией о дорожной карте
### Пример запроса
	WriteJsnRoadmap U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <3> <{"roadmapZero":[{"date":"1","how":"2","percent":30},{"date":"3","how":"4","percent":30},{"date":"5","how":"6","percent":40}],"roadmapNow":[{"date":"1","how":"2","percent":30},{"date":"3","how":"4","percent":30},{"date":"5","how":"6","percent":40}],"days":180,"countChanges":0}> 
### Ответ от сервера
	_WriteJsnRoadmap ok

GetOwnerIcoIDs     
-------------------------
GetOwnerIcoIDs - команда для получения всех стартапов текущего пользователя
	GetOwnerIcoIDs NUMBERCONNECT <UKEY>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
### Пример запроса
	GetOwnerIcoIDs U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw
### Ответ от сервера
	_GetOwnerIcoIDs 1 <3>

CreateICOToken     
-------------------------
CreateICOToken - команда для начисления пользователю токенов определённого стартапа
	CreateICOToken NUMBERCONNECT <UKEY> <IDSTARTUP> <COUNT>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
COUNT - количество токенов
### Пример запроса
	CreateICOToken U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 2 1
### Ответ от сервера
	_CreateICOToken ok

SendFrom      
-------------------------
SendFrom - команда перевода криптовалюты
	SendFrom NUMBERCONNECT <UKEY> <ADDRESS> <AMOUNT> <NUMCONF> <PORT>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
ADDRESS - адрес кошелька
AMOUNT - сумма перевода
NUMCONF - количество подтверждений
PORT - порт кошелька для перевода
*18332 - биткоин кошелёк
*19332 - лайткоин кошелёк
### Пример запроса
	SendFrom U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw miMU1QhDeupNwXc8ANeEfq7s6aRWxqyW8w 0.76923077 6 19332
### Ответ от сервера
	SendFrom ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw {"result":"50bef99139a9a4494b19d18d4d28449949ed46ac1feff2e708a262682dffb4d1","error":null,"id":"BTCExposed"}

CreateOffer1      
-------------------------
CreateOffer1 - создание предложения на бирже для продажи токена
	CreateOffer1 NUMBERCONNECT <UKEY> <IDSTARTUP> <1> <CRYPTO> <SELLAMOUNT> <AMOUNT> <ADDRESS>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
CRYPTO - за какую валюту происходит продажа
*0 - BTC
*1 - LTC
SELLAMOUNT - количество продаваемых токенов
AMOUNT - цена продажи
ADDRESS - адрес кошелька на которые будет поступление, в случае покупки на бирже
### Пример запроса
	CreateOffer1 U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <2> <1> <1> <1> <1> <mn5p53QL1zRH2tu5p6mYPYDJdS7Q9SUbTt>
### Ответ от сервера
	_CreateOffer1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw ok

RemoveOfferToken      
-------------------------
RemoveOfferToken - Удаление предложения с биржи
	RemoveOfferToken NUMBERCONNECT <UKEY> <IDOFFER>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDOFFER - идентификатор предложения
### Пример запроса
	RemoveOfferToken U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 13
### Ответ от сервера
	_RemoveOfferToken ok

ReplaceJProfile      
-------------------------
ReplaceJProfile - редактирование информации о пользователе
	ReplaceJProfile NUMBERCONNECT <UKEY> <IDUSER> <INFOARRAY>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDUSER - идентификатор пользователя
INFOARRAY - json массив с данными
### Пример запроса
	ReplaceJProfile U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <3> <{"email":"mail@mail.com","phone":"+79999999999","lName":"РњРёР»СЋС‚РёРЅ","name":"РђРЅРґСЂРµР№","city":"РљР°Р·Р°РЅСЊ","refer":0}>
### Ответ от сервера
	_ReplaceJProfile ok

GetJProfile      
-------------------------
GetJProfile - получение информации о пользователе
	GetJProfile NUMBERCONNECT <UKEY> <IDUSER>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDUSER - идентификатор пользователя
### Пример запроса
	GetJProfile U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 3
### Ответ от сервера
	_GetJProfile <{"email":"mail@mail.com","phone":"+79999999999","lName":"РњРёР»СЋС‚РёРЅ","name":"РђРЅРґСЂРµР№","city":"РљР°Р·Р°РЅСЊ","refer":0}>

GetJsnRoadmap       
-------------------------
GetJsnRoadmap - получение информации о дорожной карте
	GetJsnRoadmap NUMBERCONNECT <UKEY> <IDSTARTUP>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
### Пример запроса
	GetJsnRoadmap U295 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 2
### Ответ от сервера
	_GetJsnRoadmap ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <{"roadmapZero":[{"date":"37","how":"РєСѓРїР»РµРЅС‹ Р±СѓС‚С‹Р»Рё Рё РїРѕРіСЂСѓР¶РµРЅС‹ РІ С‚РµР»РµР¶РєСѓ ","percent":30},{"date":"45","how":"РґРѕР±РёСЂР°РµРјСЃСЏ РЅР° РІРµР»РѕСЃРёРїРµРґРµ","percent":20},{"date":"72","how":"СЃРѕР±РёСЂР°РµРј РІРѕР·РґСѓС…, РєР°СЂР°Р±РєР°РµРјСЃСЏ РІ РіРѕСЂС‹","percent":40},{"date":"3","how":"РђС€РѕС‚ С‚РѕСЂРіСѓРµС‚ РІРѕР·РґСѓС…РѕРј РЅР° С†РµРЅС‚СЂР°Р»СЊРЅРѕРј СЂС‹РЅРєРµ","percent":10}],"roadmapNow":[{"date":"37","how":"РєСѓРїР»РµРЅС‹ Р±СѓС‚С‹Р»Рё Рё РїРѕРіСЂСѓР¶РµРЅС‹ РІ С‚РµР»РµР¶РєСѓ ","percent":30},{"date":"45","how":"РґРѕР±РёСЂР°РµРјСЃСЏ РЅР° РІРµР»РѕСЃРёРїРµРґРµ","percent":20},{"date":"72","how":"СЃРѕР±РёСЂР°РµРј РІРѕР·РґСѓС…, РєР°СЂР°Р±РєР°РµРјСЃСЏ РІ РіРѕСЂС‹","percent":40},{"date":"3","how":"РђС€РѕС‚ С‚РѕСЂРіСѓРµС‚ РІРѕР·РґСѓС…РѕРј РЅР° С†РµРЅС‚СЂР°Р»СЊРЅРѕРј СЂС‹РЅРєРµ","percent":10}],"days":180,"countChanges":0}>

ReplaceICOP       
-------------------------
ReplaceICOP - изменение информации о стартапе
	ReplaceICOP NUMBERCONNECT <UKEY> <1> <1> <STARTUPNAME> <555555> <1000> <43426> <INFOARRAY> <TEAMARRAY> <ADVARRAY> <DOCSARRAY> <IDSTARTUP>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
STARTUPNAME - название стартапа
INFOARRAY - json массив с информацией о стартапе
TEAMARRAY - json массив с командой стартапа
ADVARRAY - json массив с помощниками стартапа
DOCSARRAY - json массив с документами стартапа
IDSTARTUP - идентификатор стартапа
### Пример запроса
	ReplaceICOP U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <1> <1> <РќР°РёРјРµРЅРѕРІР°РЅРёРµ РїСЂРѕРµРєС‚Р°> <555555> <1000> <43426> <{"name":"РќР°РёРјРµРЅРѕРІР°РЅРёРµ РїСЂРѕРµРєС‚Р°","shortDesc":"РљСЂР°С‚РєРѕРµ РѕРїРёСЃР°РЅРёРµ РїСЂРѕРµРєС‚Р°","fullDesc":"&lt;p&gt;РџРѕРґСЂРѕР±РЅРѕРµ РѕРїРёСЃР°РЅРёРµ РїСЂРѕРµРєС‚Р°&lt;\/p&gt;\r\n","problems":"РџСЂРѕР±Р»РµРјС‹, РєРѕС‚РѕСЂС‹Рµ СЂРµС€Р°РµС‚ РїСЂРѕРµРєС‚","problemsHow":"РљР°РєРёРј РѕР±СЂР°Р·РѕРј СЂРµС€Р°СЋС‚СЃСЏ СЌС‚Рё РїСЂРѕР±Р»РµРјС‹","tech":"РўРµС…РЅРѕР»РѕРіРёСЏ \/ РѕРїРёСЃР°РЅРёРµ РїСЂРѕРґСѓРєС‚Р°","projectState":"3","video":"https:\/\/www.youtube.com\/watch?v=-RZjoHj78fM&amp;t=4s","website":"http:\/\/desdao.com","uName":"РЎРѕРєРѕР»РѕРІ Р’Р»Р°РґРёСЃР»Р°РІ Р’Р°Р»РµСЂСЊРµРІРёС‡","uPhone":"+70123456789","uSLink":"https%3A%2F%2Fwww.instagram.com%2Fbubblegumoff%2F","roadmap":[],"whoCreate":3,"btcAddress":"moA9iaFviQQoYqkGKC536ZaBHmcCZKRME9","ltcAddress":"n4YbLM18X8H2FrBeQ8To3K6QK7wM995B1k"}> <[{"name":"1","position":"2","contacts":[],"photo":"http:\/\/desdao.com\/template\/images\/des\/slider-1-photo.png","desc":""}]> <[{"name":"1","position":"2","contacts":[],"photo":"http%3A%2F%2Fdesdao.com%2Ftemplate%2Fimages%2Fuser.png","desc":""}]> <null> <3>
### Ответ от сервера
	_ReplaceICOP 3

SetModerate        
-------------------------
SetModerate - присваивает стартапу статус прошедшего модерацию
	SetModerate NUMBERCONNECT <UKEY> <IDSTARTUP>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
### Пример запроса
	SetModerate U346 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 3
### Ответ от сервера
	_SetModerate ok

Registration        
-------------------------
Registration - регистрация пользователя
	Registration NUMBERCONNECT <PHONE> <EMAIL> <INFOARRAY>
### Параметры
NUMBERCONNECT - номер соединения
PHONE - телефон пользователя
EMAIL - почтовый адрес пользователя
INFOARRAY - json массив с информацией о пользователе
### Пример запроса
	Registration U1 +79999999998 mail@mail.ru <{"email":"mail@mail.ru","phone":"+79999999998","lName":"Ivanov","name":"Ivan","city":"Kazan","refer":0}>
### Ответ от сервера
	_Registration 37 9006 49

SetLike         
-------------------------
SetLike - голосование за продолжение ICO
	SetLike NUMBERCONNECT <UKEY> <IDSTARTUP> <STATUS>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
STATUS - статус
*1 - за
*2 - против
### Пример запроса
	SetLike U357 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 2 2
### Ответ от сервера
	_SetLike 2

GetOffers1         
-------------------------
GetOffers1 - возвращает строки биржи
	GetOffers1 NUMBERCONNECT <UKEY> *
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
### Пример запроса
	GetOffers1 U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw * 
### Ответ от сервера
	_GetOffers1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw <1 1 1 2 5 1527276669 6 mppWTyUDFC4avFF9rFv2KFBbh2u2k6cWy3>  <1 1 1 12 3 1527276860 7 mppWTyUDFC4avFF9rFv2KFBbh2u2k6cWy3>  <1 1 0 20 13 1527277377 8 mhxS96RPyrJPKcbvjzHLzrKpX3jh5Zyvos>  <1 1 1 1 9.99999974737875E-5 1527278768 9 mppWTyUDFC4avFF9rFv2KFBbh2u2k6cWy3>  <1 1 1 88 4 1527518915 10 mppWTyUDFC4avFF9rFv2KFBbh2u2k6cWy3>  <2 1 0 100 9.99999974737875E-5 1528190295 12 mpJfD7cfoBNYAjeQeKSbECRr4zHLDaddDy>

GetInfoVotes         
-------------------------
GetInfoVotes - возвращает информацию о проголосовавших
	GetInfoVotes NUMBERCONNECT <UKEY> <IDSTARTUP>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
### Пример запроса
	GetInfoVotes U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 2
### Ответ от сервера
	_GetInfoVotes 3 <17 0,2 1> <16 0,1 1> <3 0,2 2>

GetInvestCount         
-------------------------
GetInvestCount - возвращает количество инвесторов стартапа
	GetInvestCount NUMBERCONNECT <UKEY> <IDSTARTUP>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
### Пример запроса
	GetInvestCount U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 2
### Ответ от сервера
	_GetInvestCount 2

GetIcoVotes          
-------------------------
GetIcoVotes - возвращает количество проголосовавших против
	GetIcoVotes NUMBERCONNECT <UKEY> <IDSTARTUP>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
IDSTARTUP - идентификатор стартапа
### Пример запроса
	GetIcoVotes U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 2
### Ответ от сервера
	_GetIcoVotes <2>

GetCloudAdreses           
-------------------------
GetCloudAdreses - возвращает все адресы кошелька пользователя
	GetCloudAdreses NUMBERCONNECT <UKEY> * <PORT>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
PORT - номер порта по которому можно получить информацию по кошельку
*18332 - биткоин кошелёк
*19332 - лайткоин кошелёк
### Пример запроса
	GetCloudAdreses U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw * 18332
### Ответ от сервера
	GetCloudAdreses ipayTT2veqs1MlvNw0spUtfRa0wc7Xetrt {"result":["mgoXurLZpGjf5zkbdK84hBGwMTgPM2hv4f","mk4PKva46EPE1V1KBcUcbuAyhAPV3BapDc","moA9iaFviQQoYqkGKC536ZaBHmcCZKRME9","mwwgwLsYfjb7SjBFzPSu9FwFivxVXkGw6A","n2E7muV6doSPFjCEvFYYwzPwYHBEH2uKhL","n32DXE53Fqjj7bWCyQmrkf7k1KVnb6yawU"],"error":null,"id":"BTCExposed"}

listtransactions           
-------------------------
listtransactions - возвращает все операции пользователя по кошельку
	GetCloudAdreses NUMBERCONNECT <UKEY> * <PORT>
### Параметры
NUMBERCONNECT - номер соединения
UKEY - уникальный ключ, который получить можно при помощи команды CheckPin
PORT - номер порта по которому можно получить информацию по кошельку
*18332 - биткоин кошелёк
*19332 - лайткоин кошелёк
### Пример запроса
	listtransactions U1 ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw * 18332
### Ответ от сервера
	listtransactions ipavTkLooTa2UBbh4vJcx0t1ixgiwMj9Mw 8 <n2E7muV6doSPFjCEvFYYwzPwYHBEH2uKhL receive 1525959165 0.10000000 27461 7476ef974eee12b8504f2cf57c5c5b0da38c88761c44c1e65592e3963055b5de> <n2E7muV6doSPFjCEvFYYwzPwYHBEH2uKhL receive 1526135046 1.56693640 27204 6d1b829026f69d4649a9ff79780316639faf29186e4e2e95d308d4afa9418eb5> <n2E7muV6doSPFjCEvFYYwzPwYHBEH2uKhL receive 1526135061 1.20110978 27204 c740c24e72e649c8800a33681c5c5ba82587d78efcd7e64693ba794ccb2deb85> <n32DXE53Fqjj7bWCyQmrkf7k1KVnb6yawU receive 1526135370 0.53659787 27204 caf12b8bed5966f0059a3c2ec75025f74e9f3604c25eae78e1d5b414acc5e5b6> <n32DXE53Fqjj7bWCyQmrkf7k1KVnb6yawU receive 1526135499 1.11180493 27204 9ab76479f33e531ba6e9b47431bafb9524a8e914e6afbb7dc7503665d73714a7> <n32DXE53Fqjj7bWCyQmrkf7k1KVnb6yawU receive 1526136098 3.40465242 27203 b0d9ad62ed6fd71185dd588ac45610f66dea32f1555557f5d519d35a70afbc4e> <mppWTyUDFC4avFF9rFv2KFBbh2u2k6cWy3 send 1526901633 -2.00000000 26368 cfe861ab46edffd802d081d7ae64fd665e36ab56d1a672c4dfc299fbc4a2992b> <mppWTyUDFC4avFF9rFv2KFBbh2u2k6cWy3 send 1526901667 -2.00000000 26368 c51ce9d857477c4e8ef65133a0b0daf963241aeb4b8394522a3da315a9f96f2e>